#!/bin/bash

# Esse bash script mostra uma forma de capturar e modificar um
# elemento em um SVG sem quebrar sua estrutura e identação original

# Cria variáveis base e incorpora funções úteis:
. "$( dirname $0 )/ink-bash-base.sh"


# Pega apenas o primeiro argumento ID, caso mais de um seja enviado:
id="$( getArg --id "$@" | head -n 1 )"
# Pega o argumento de cor:
cor="$( getArg --cor "$@" )"

# Inicia o parcer que criará um arquivo temporário onde outras
# funções poderão trabalhar:
startSvgParser

# Seleciona um elemento pelo ID e já modifica sua cor:
elemento="$( getElementById $id | setStyle fill "$cor" )"

# Pega o valor do atributo transformação do elemento selecionado:
transf="$( echo "$elemento" | getAtribute transform )"

# Modifica o atributo transform adicionando uma transformação a lista:
elemento="$( echo "$elemento" | setAtribute transform "$transf rotate(10)" )"

# Adiciona um novo atributo (sei que o atributo teste não existia):
elemento="$( echo "$elemento" | setAtribute teste 'valor bli' )"

# Substitui o elemento com ID $id pelo elemento modificado:
echo "$elemento" | replaceElementById $id

# Adiciona código SVG extra, como último elemento filho de <svg>:
echo -n '<text>
Isso é um Teste!
</text>' | appendSvgCode

# Adiciona código SVG extra, antes de certa tag:
echo -n "<text>Texto antes do id $id</text>" | addSvgCodeBeforeTagWithId $id

# Adiciona código SVG extra, depois de certa tag:
echo -n "<text>Texto depois do id $id</text>" | addSvgCodeAfterTagWithId $id

# Imprime o código SVG para o Inkscape aplicar as modificações:
echo -n '#####'
printSVG
echo '#####'

# Desfaz tudo o que for temporário para concluir o trabalho:
endSvgParser
