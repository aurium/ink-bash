#!/bin/bash
#
#   ink-bash-base.sh
#   The base actions to handle SVG and meke Inkscape extensions in Bash Script
#   Copyright (C) 2006 Aurélio A. Heckert <aurium@gmail.com>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
#   You can also find the GPL hire: http://www.gnu.org/licenses/gpl.html
#   Você pode encontar a GPL aqui:  http://www.gnu.org/licenses/gpl.html


# Get the SVG File argument and put on the $svgFile variable:
for arg in "$@"; do
  if ! ( echo $arg | grep -q '^-.*' 2>/dev/null ); then
    svgFile=$arg
  fi
done


function createMark {
  # Create a mark to replace someting on the SVG code and enable the edition.
  # This function will test if the mark exists on the SVG before and will
  # apped some Xs to the mark while the mark exists.
  X=''
  while grep -i "{$1$X}" "$svgFile"; do
    X="X$X"
  done
  echo "{$1$X}"
}

# A mark for line breaks:
br="$(  createMark BR  )"
# A mark for ':
apo="$( createMark APO )"


# Search for an argument on a argument list and return its value:
# (If exists more the one argument whit the same name will
# return a list of values with each in a line.)
function getArg {
  first=1
  finded=0
  findArg="$1"
  for arg in "$@"; do
    if [ $first = 1 ]; then
      # the first argument is the arg to find. So... nothing to do
      first=0
    else
      if echo $arg | grep -q "^$findArg=" 2>/dev/null; then
        [ $finded = 1 ] && echo
        echo -n $arg | sed 's/^[^=]*=\("\?\)\(.*\?\)\1$/\2/'
        finded=1
      fi
    fi
  done
}


# working files:
svgTMP=$(  mktemp )
svgTMP2=$( mktemp )


# Scape a SVG code replaceing line breaks and apostrofes with
# markeds and puting each tag in a line only:
function scapeSvgCode {
  # protect each line and mark the end with {BR}:
  sed "s/'/$apo/g; s/^/'/; s/$/$br'/" |
  xargs -L 1 echo -n |  # cut out the original \n of the SVG code
  sed "s/$br$//; s/\(<[a-z]\+\|<!\|<\/\)/\n\1/g; s/>\(.\)/>\n\1/g"
  # separate each tag in a line ant put this on $svgTMP file.
}

function unscapeSvgCode {
  sed "s/^\|$/'/g; s/^'$/''/" |  # protect each line for the echo
  xargs -L 1 echo -n |           # cut out all \n
  sed "s/$apo/'/g; s/$br/\n/g"   # replace marks by its original value
}


# Make a temp file with scaped code:
function startSvgParser {
  cat "$svgFile" | scapeSvgCode > $svgTMP
}


# Clear the temp files.
function endSvgParser {
  rm $svgTMP $svgTMP2
}


# Find an element by tis ID and return it:
function getElementById {
  grep " id *= *\"$1\"" $svgTMP
}

#
function getElementsByTagName {
  grep "<$1[^-_:a-zA-Z0-9]" $svgTMP
}

# Sets a Style atribute (Will create if don't exists):
function setStyle {
  element=$( mktemp )
  if ( tee $element | grep -q " style= *\( *\" *\| *\"[^\"]*; *\)$1:[^;]*" ); then
    cat $element | sed "s/ style= *\( *\" *\| *\"[^\"]*; *\)$1:[^;]*/ style=\1$1:$2/g"
  else
    cat $element | sed "s/ style= *\( *\"[^\"]*\)\"/ style=\1;$1:$2\"/g"
  fi
  rm $element
}

# Gets an element Atribute:
function getAtribute {
  grep " $1 *= *\"" | sed "s/^.* $1 *= *\"\([^\"]*\)\".*$/\1/g"
}

# Sets an element Atribute (Will create if don't exists):
function setAtribute {
  element=$( mktemp )
  if ( tee $element | grep -q " $1 *= *\"" ); then
    cat $element | sed "s/ $1 *= *\"[^\"]*\"/ $1=\"$2\"/g"
  else
    cat $element | sed "s/\(\/\?\)>/ $1=\"$2\"\1>/g"
  fi
  rm $element
}


# Repace the element with some ID by an recived element:
function replaceElementById {
  element=$( sed 's/\\/\\\\/g; s/\//\\\//g' )
  sed "s/^.* id *= *\"$1\".*$/$element/" $svgTMP  > $svgTMP2
  cp $svgTMP2 $svgTMP
}


startTag="$( createMark ST )"
endTag="$( createMark ET )"

# Add extra SVG code, as the last child of <svg>:
function appendSvgCode {
  code=$( sed "s/</$startTag/g; s/>/$endTag/g" |
          scapeSvgCode | sed 's/\\/\\\\/g; s/\//\\\//g' )
  sed "s/\(.*<\/svg>.*\)/$code\1/; s/$startTag/</g; s/$endTag/>/g" $svgTMP |
  unscapeSvgCode | scapeSvgCode > $svgTMP2
  cp $svgTMP2 $svgTMP
}

# Add extra SVG code, before some tag:
function addSvgCodeBeforeTagWithId {
  code=$( sed "s/</$startTag/g; s/>/$endTag/g" |
          scapeSvgCode | sed 's/\\/\\\\/g; s/\//\\\//g' )
  sed "s/\(^.* id *= *\"$1\".*\)$/$code\1/; s/$startTag/</g; s/$endTag/>/g" $svgTMP |
  unscapeSvgCode | scapeSvgCode > $svgTMP2
  cp $svgTMP2 $svgTMP
}

# Add extra SVG code, after some tag:
function addSvgCodeAfterTagWithId {
  code=$( sed "s/</$startTag/g; s/>/$endTag/g" |
          scapeSvgCode | sed 's/\\/\\\\/g; s/\//\\\//g' )
  sed "s/\(^.* id *= *\"$1\".*\)\($br\)\?$/\1$code\2/; s/$startTag/</g; s/$endTag/>/g" $svgTMP |
  unscapeSvgCode | scapeSvgCode > $svgTMP2
  cp $svgTMP2 $svgTMP
}

function mm2px {
  echo -n $(( ( $1 * 3543307115 ) / 1000000000 ))
}

function px2mm {
  echo -n $(( ( $1 * 1000000000 ) / 3543307115 ))
}

# Show an information window for the user:
function alert {
  str="$1"
  script="$( basename $0 )"
  if ( echo "$script" | grep -q '.\.' ); then
    script="$( echo "$script" | sed 's/.[^.]*$//' )"
  fi
  zenity --info --title="$script - Ink-Bash" --text="$str"
}

# Show the pure SVG code:
function printSVG {
  cat $svgTMP | unscapeSvgCode
}
